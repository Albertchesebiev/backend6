<?php
include('../global_func.php');
// ID пользователей для удаления из БД
$rm_users = [];
foreach ($_COOKIE as $key => $value) {
  if (strstr($key, 'rm_user_')) {
    $rm_users[] = $value;
  }
}
if (empty($rm_users)) {
  // print 'empty';
  header('Location: ../views/admin.php');
} else {
  // print '!empty';
  // очищаем куки
  foreach ($_COOKIE as $key => $value) {
    if (strstr($key, 'rm_user_')) {
      setcookie($key, '', 1, "/backend6/");
    }
  }
  // удаляем отмеченных юзеров из БД
  try {
    $query = "DELETE FROM user5 WHERE ";
    foreach ($rm_users as $id) {
      if ($id == end($rm_users)) {
        $query .= 'id = ' . $id;
      } else {
        $query .= 'id = ' . $id . ' OR ';
      }
    }
    $db = connectToDB();
    $stmt = $db->prepare($query);
    $stmt->execute();
  } catch (PDOException $e) {
    print $e->getMessage();
  }
  header('Location: ../views/admin.php');
}
